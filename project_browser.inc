<?php

/**
 * @file
 * Various functions that are required by project_browser.
 */

// @todo - Remove these once http://drupal.org/node/1846078 is committed.
use Drupal\Core\Updater\Updater;
use Drupal\Core\FileTransfer\Local;

/**
 * Builds the themed install list form.
 *
 * @return array
 *   A render array for the install queue block.
 */
function project_browser_get_install_list() {
  $queued_projects = project_browser_get_queued_projects();

  // Show a list of the queued projects.
  return theme('project_browser_install_queue', array('projects' => $queued_projects));
}

/**
 * Form constructor for the install button for the Install Queue block.
 *
 * Since the selected projects are stored in the $_SESSION variable,
 * no real processing is done, we just redirect to the install/select_versions
 * page.
 *
 * @see project_browser_preprocess_project_browser_install_queue()
 * @ingroup forms
 */
function project_browser_install_button_form($form, &$form_state) {
  $form['#attributes']['id'] = 'project-browser-install-button-form';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Install',
    '#button_type' => 'primary',
  );

  $form['#action'] = url('admin/modules/project-browser/install/select_versions');

  return $form;
}

/**
 * Form constructor for the filters form.
 *
 * This includes categories and the string search box, and the $type is stored.
 *
 * @param string $type
 *   The type of project (module or theme).
 *
 * @see project_browser_filters_form_submit()
 * @ingroup forms
 */
function project_browser_filters_form($form, &$form_state, $type) {
  $form['search_text'] = array(
    '#type' => 'textfield',
    '#size' => '25',
    '#title' => t('Search'),
    '#default_value' => isset($_SESSION['project_browser_text_filter_' . $type]) ? $_SESSION['project_browser_text_filter_' . $type] : '',
  );

  // Add the category filter if there are categories.
  if ($categories = project_browser_get_categories($type)) {
    array_unshift($categories, t('-- Any --'));
    $form['category'] = array(
      '#type' => 'select',
      '#title' => t('Category'),
      '#options' => $categories,
      '#default_value' => isset($_SESSION['project_browser_category_filter_' . $type]) ? $_SESSION['project_browser_category_filter_' . $type] : NULL,
    );
  }

  $sort_options = project_browser_get_sort_options();
  $current_order_by = isset($_SESSION['project_browser_order_by_filter_' . $type]) ? $_SESSION['project_browser_order_by_filter_' . $type] : 'score';
  $current_sort = isset($_SESSION['project_browser_sort_filter_' . $type]) ? $_SESSION['project_browser_sort_filter_' . $type] : 'desc';
  $form['sort'] = array(
    '#type' => 'item',
    '#title' => t('Sort by'),
    '#markup' => project_browser_get_sort_widget($sort_options, $current_order_by, $current_sort),
  );

  $form['project_type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );

  return $form;
}

/**
 * Form submission handler for project_browser_filters_form().
 *
 * All that we do here is store the selected category and search string
 * in the $_SESSION variable.
 */
function project_browser_filters_form_submit($form, &$form_state) {
  $type = $form_state['values']['project_type'];
  if (!empty($form_state['values']['category'])) {
    $_SESSION['project_browser_category_filter_' . $type] = array($form_state['values']['category'] => $form_state['values']['category']);
  }
  else {
    $_SESSION['project_browser_category_filter_' . $type] = array();
  }
  $_SESSION['project_browser_text_filter_' . $type] = $form_state['values']['search_text'];
}

/**
 * Builds a themed sort widget for the results.
 *
 * These are links which can be clicked/toggled to select and change direction.
 *
 * @param array $sort_options
 *   An array of sort options.
 * @param string $current_sort_option
 *   The currently selected sort option.
 * @param string $current_sort_direction
 *   The currently selected sort direction.
 *
 * @return string
 *   A themed list of sort options.
 */
function project_browser_get_sort_widget(array $sort_options, $current_sort_option, $current_sort_direction) {
  $sort_list = array();

  foreach ($sort_options as $sort_option) {
    $classes = array();
    $query = array(
      'order_by' => $sort_option['method'],
      'sort' => $sort_option['default_sort'],
    );

    // If the sort option is currently active, handle it differently.
    if ($current_sort_option == $sort_option['method']) {
      $classes[] = 'sort-active';
      $classes[] = 'sort-' . $current_sort_direction;

      // Set the direction of the sort link to the opposite of what it
      // currently is.
      if ($current_sort_direction == $query['sort']) {
        if ($query['sort'] == 'desc') {
          $query['sort'] = 'asc';
        }
        else {
          $query['sort'] = 'desc';
        }
      }

      $sort_list[] = array(
        '#markup' => l($sort_option['name'], current_path(), array('query' => $query, 'class' => array())) . theme('tablesort_indicator', array('style' => $query['sort'])),
        '#wrapper_attributes' => array(
          'class' => $classes,
        ),
      );
    }
    else {
      $classes[] = 'sort-inactive';
      $sort_list[] = array(
        '#markup' => l($sort_option['name'], current_path(), array('query' => $query, 'class' => array())),
        '#wrapper_attributes' => array(
          'class' => $classes,
        ),
      );
    }
  }

  return theme('item_list', array(
      'items' => $sort_list,
      'type' => 'ul',
      'attributes' => array('class' => array('project-browser-sort-list'))
    )
  );
}

/**
 * Builds a themed widget to select the server.
 *
 * This is only called if there are more than one server enabled in the
 * settings.
 *
 * @param array $servers
 *   An array of servers that should be available as options.
 * @param string $current_server
 *   The currently selected server.
 *
 * @return string
 *   A themed server select widget.
 */
function project_browser_get_server_widget(array $servers, $current_server) {
  $list = array();
  $list[] = array(
    'data' => t('Repository:'),
    'class' => array('server-header')
  );
  $current_path = drupal_get_path_alias($_GET['q']);

  $i = 0;

  foreach ($servers as $url => $server) {
    $classes = array();
    $query = array(
      'repository' => $i,
    );

    // If the sort option is currently active, handle it differently.
    if ($current_server == $i) {
      $classes[] = 'server-active';
    }
    else {
      $classes[] = 'server-inactive';
    }

    $list[] = array(
      '#markup' => l($server['name'], $current_path, array('query' => $query, 'class' => array())),
      '#wrapper_attributes' => array(
        'class' => $classes,
      ),
    );

    $i += 1;
  }

  return theme('item_list', array(
      'items' => $list,
      'type' => 'ul',
      'attributes' => array('class' => array('project-browser-servers-list'))
    )
  );
}

/**
 * Builds and returns an array of sort options, keyed by method.
 *
 * @param bool $full
 *   (optional) Set this to TRUE if you want to get all of the supported sort
 *   methods. Defaults to FALSE.
 *
 * @return array
 *   An array of sort options, keyed by method.
 */
function project_browser_get_sort_options($full = FALSE) {
  $sort_options = array(
    'score' => array('method' => 'score', 'name' => t('Relevancy'), 'default_sort' => 'desc'),
    'usage' => array('method' => 'usage', 'name' => t('Most installed'), 'default_sort' => 'desc'),
    'title' => array('method' => 'title', 'name' => t('Title'), 'default_sort' => 'asc'),
    'name' => array('method' => 'name', 'name' => t('Author'), 'default_sort' => 'asc'),
    'latest_release' => array('method' => 'latest_release', 'name' => t('Latest release'), 'default_sort' => 'desc'),
  );

  if ($full) {
    $sort_options['type'] = array(
      'method' => 'type',
      'name' => t('Type'),
      'default_sort' => 'asc'
    );
    $sort_options['created'] = array(
      'method' => 'created',
      'name' => t('Date created'),
      'default_sort' => 'asc'
    );
    $sort_options['latest_activity'] = array(
      'method' => 'latest_activity',
      'name' => t('Latest build'),
      'default_sort' => 'desc'
    );
  }

  return $sort_options;
}

/**
 * Builds an array of all available categories for a project type.
 *
 * @param string $type
 *   The type of project to get the categories for. Example: 'module' or
 *   'theme'.
 *
 * @return array|false
 *   Array containing all available categories or FALSE if no categories.
 */
function project_browser_get_categories($type) {
  $categories = array();

  // Get which server to use from $_SESSION.
  if (isset($_SESSION['project_browser_server_filter'])) {
    $use_server = $_SESSION['project_browser_server_filter'];
  }
  else {
    $use_server = 0;
  }

  $categories_raw = project_browser_fetch_categories($type, $use_server);

  if (is_array($categories_raw) AND !empty($categories_raw)) {
    foreach ($categories_raw as $url => $cats) {
      foreach ($cats as $key => $value) {
        // Create a new key so that there are no duplicate categories from
        // different sites.
        $new_key = preg_replace('/[^a-z0-9_]+/', '_', strtolower($value));
        $categories[$new_key] = $value;
      }
    }
  }

  if (is_array($categories) AND !empty($categories)) {
    ksort($categories);

    return $categories;
  }
  return FALSE;
}

/**
 * Prepares the categories for sending to the servers as filters.
 *
 * @param array $raw_cats
 *   An array of categories from $form_state['values'].
 * @param string $type
 *   The type of project to prepare the categories for, eg 'module' or 'theme'.
 *
 * @return array
 *   An array of server categories, keyed by server url.
 */
function project_browser_prepare_categories(array $raw_cats, $type) {
  $categories = project_browser_fetch_categories($type);

  // Set the value of the categories to true if it is selected.
  foreach ($categories as $url => $cats) {
    foreach ($cats as $key => $value) {
      $new_key = preg_replace('/[^a-z0-9_]+/', '_', strtolower($value));
      if (isset($raw_cats[$new_key]) AND $raw_cats[$new_key]) {
        $categories[$url][$key] = TRUE;
      }
      else {
        unset($categories[$url][$key]);
      }
    }

    // Unset the parent if there are no children.
    if (empty($categories[$url])) {
      unset($categories[$url]);
    }
  }

  return $categories;
}

/**
 * Checks if a project is enabled.
 *
 * @param string $type
 *   The type of project. Could be 'theme' or 'module'.
 * @param string $name
 *   The short name of the project.
 *
 * @return bool
 *   TRUE if the project is enabled, FALSE otherwise.
 */
function _project_browser_is_project_enabled($type, $name) {
  switch ($type) {
    case 'module':
      return module_exists($name);
      break;

    case 'theme':
      $themes = list_themes();
      return isset($themes[$name]);
      break;
  }
  return FALSE;
}

/**
 * Gets the currently listed projects from the session.
 *
 * @return array
 *   An array of listed projects from the $_SESSION variable.
 */
function project_browser_get_listed_projects() {
  if (isset($_SESSION['project_browser_listed_projects'])) {
    return $_SESSION['project_browser_listed_projects'];
  }

  return array();
}

/**
 * Gets the currently queued projects from the $_SESSION variable.
 *
 * @param string|null $type
 *   (optional) The type of project (module or theme). Defaults to NULL, which
 *   will return projects of all types.
 *
 * @return array
 *   An array of projects that are queued for install.
 */
function project_browser_get_queued_projects($type = NULL) {
  $projects = array();

  if (isset($_SESSION['project_browser_install_list'])) {
    foreach ($_SESSION['project_browser_install_list'] as $project) {
      if (is_array($project) AND !empty($project)) {
        if (isset($type) AND $type != $project['type']) {
          continue;
        }
        else {
          $projects[$project['name']] = $project;
        }
      }
    }
  }

  return $projects;
}

/**
 * Gets a release from a project and a release_name.
 *
 * @param string $release_name
 *   The name of the release, such as '7.x-1.2'.
 * @param array $project
 *   The $project data array.
 *
 * @return array|false
 *   The release data array or FALSE if the release doesn't exist.
 */
function project_browser_get_release($release_name, array $project) {
  $release_data = project_browser_get_project_release_data($project);

  return isset($release_data['releases'][$release_name]) ? $release_data['releases'][$release_name] : FALSE;
}

/**
 * Gets the newly installed projects from the session.
 *
 * @return array
 *   An array of all of the newly installed projects.
 */
function project_browser_get_installed_projects() {
  $projects = array();

  if (isset($_SESSION['project_browser_installed_projects'])) {
    foreach ($_SESSION['project_browser_installed_projects'] as $project) {
      if (is_array($project) AND !empty($project)) {
        $projects[$project['name']] = $project;
      }
    }
  }

  return $projects;
}

/**
 * Adds a project to the install queue $_SESSION variable.
 *
 * @param array $project
 *   An array of $project data for a single project.
 */
function project_browser_install_queue_add(array $project) {
  $_SESSION['project_browser_install_list'][$project['name']] = $project;
}

/**
 * Removes a project from the install queue $_SESSION variable.
 *
 * @param string $project_name
 *   The name of the project to remove, such as 'views'.
 */
function project_browser_install_queue_remove($project_name) {
  if (isset($_SESSION['project_browser_install_list'][$project_name])) {
    unset($_SESSION['project_browser_install_list'][$project_name]);
  }
}

/**
 * Gets the currently queued releases from the $_SESSION variable.
 *
 * @return array
 *   An array of the currently selected releases.
 */
function project_browser_get_queued_releases() {
  $releases = array();

  if (isset($_SESSION['project_browser_install_releases_list'])) {
    foreach ($_SESSION['project_browser_install_releases_list'] as $short_name => $info) {
      if (is_array($info['project']) AND !empty($info['project'])) {
        $releases[$short_name] = $info;
      }
    }
  }

  return $releases;
}

/**
 * Fetches results from the servers based on the parameters passed in.
 *
 * @param array $filters
 *   An associative array of queries to use to filter results, containing:
 *   - version: The Major Version of Drupal that is running on the Client.
 *     Example: '7' or '8'.
 *   - text: The text that was entered as the search query, or '' if none.
 *     Example: 'Link field'.
 *   - categories: An array of categories that were selected, if any.
 *   - type: The type of project being searched for. Example: 'module' or
 *     'theme'.
 *   - page: The zero-based page number.
 *   - requested: How many results are requested per page.
 *
 * @return array
 *   An array of results formatted like:
 *   - total: The total number of results found for the filters.
 *   - projects: An array of projects returned for this page request,
 *     containing:
 *     - KEY: A project array keyed by the machine name:
 *       - type: The type of project this is. Can be 'module' or 'theme'.
 *       - title: The title of the project.
 *       - name: The machine name of the project.
 *       - author: The author's name.
 *       - description: Long project description text.
 *       - image: Absolute url to the image, if any.
 *       - usage: How many sites are using module.
 *       - project url: Absolute url to the project page, if any.
 *       - project status url: The absolute url of the update checker,
 *         formatted like how Drupal.org Update Status does it.
 *       - last updated: UNIX Timestamp of when the project was last updated.
 *       - maintenance status: Maintenance status.
 *       - development status: Development status.
 *       - rating: A rating on a scale of 1 to 10 of the project, if available
 *       - dependencies: An array of the dependencies of this module, by
 *         project shortname, if available.
 */
function project_browser_fetch_results(array $filters) {
  $servers = project_browser_get_servers($filters['server']);
  // Attempt to retrieve the cached version of this page.
  $cid = md5(serialize(array_merge($filters, $servers)));

  if ($cache = cache()->get($cid)) {
    return $cache->data;
  }

  $results = array(
    'projects' => array(),
    'total' => 0,
  );

  unset($filters['server']);

  foreach ($servers as $url => $server) {
    $local_filters = $filters;

    // We are not using this right now because we only expect to handle 1
    // server at a time currently.
    // $local_filters['requested'] = floor($filters['requested'] / count($servers));

    // Send only the relevant categories to the server.
    if (isset($filters['categories'])) {
      if (!isset($filters['categories'][$url])) {
        // Don't call a server for results if categories are being used, and
        // none of them belong to the server.
        continue;
      }
      $local_filters['categories'] = $filters['categories'][$url];
    }

    // Use XMLRPC if it is set.
    if ($server['method'] == 'xmlrpc') {
      $results_raw = xmlrpc($url, array(
        'project_browser_server.fetch_results' => array($local_filters),
      ));

      // Check for errors.
      if ($error = xmlrpc_error() AND $error->is_error) {
        drupal_set_message(t("Encountered an error when trying to fetch results from @name. Error @code : @message",
          array('@name' => $server['name'], '@code' => $error->code, '@message' => $error->message)));
        continue;
      }
    }

    // Use json if it is set.
    if ($server['method'] == 'json') {
      $local_filters['method'] = 'query';
      if (isset($local_filters['categories'])) {
        $local_filters['categories'] = serialize($local_filters['categories']);
      }

      $query_url = $url . '/query/' . $local_filters['type'] . '/8?' . http_build_query($local_filters, FALSE, '&');
      $response = drupal_http_request($query_url);
      if ($response->code == '200') {
        $results_raw = drupal_json_decode($response->data);
      }
      else {
        drupal_set_message(t("Encountered an error when trying to fetch results from @name. Error @code : @message",
          array('@name' => $server['name'], '@code' => $response->code, '@message' => $response->error)));
        continue;
      }
    }

    if (isset($results_raw['total'])) {
      $results['total'] += $results_raw['total'];
    }

    if (!empty($results_raw['projects']) AND is_array($results_raw['projects'])) {
      // Merge the results.
      $results['projects'] = array_merge($results['projects'], $results_raw['projects']);
    }
  }

  cache()->set($cid, $results, REQUEST_TIME + config('project_browser.settings')->get('cache_lifetime'));

  return $results;
}

/**
 * Fetches categories from the servers based on the type of project.
 *
 * @param string $type
 *   The type of project we are getting categories for. Can be 'module' or
 *   'theme'
 * @param string $use_server
 *   (optional) The server to use. Defaults to 'all'.
 *
 * @return array
 *   Returns an array of the categories.
 */
function project_browser_fetch_categories($type, $use_server = 'all') {
  $servers = project_browser_get_servers($use_server);

  // Attempt to retrieve the cached version of this page.
  $cid = md5('categories_' . $type . serialize($servers));

  if ($cache = cache()->get($cid)) {
    return $cache->data;
  }
  else {
    $categories = array();

    foreach ($servers as $url => $server) {
      $categories_raw = array();
      // Use xmlrpc if it is set.
      if ($server['method'] == 'xmlrpc') {
        $categories_raw = xmlrpc($url, array(
          'project_browser_server.fetch_categories' => array($type),
        ));

        // Check for errors.
        if ($error = xmlrpc_error() AND $error->is_error) {
          drupal_set_message(t("Encountered an error when trying to fetch categories from @name. Error @code : @message",
            array('@name' => $server['name'], '@code' => $error->code, '@message' => $error->message)));
          continue;
        }
      }

      // Use json if it is set.
      if ($server['method'] == 'json') {
        $params = array(
          'method' => 'categories',
          'type' => $type,
        );
        $response = drupal_http_request($url . '/categories/' . $type . '?' . http_build_query($params, FALSE, '&'));
        if ($response->code == '200') {
          $categories_raw = drupal_json_decode($response->data);
        }
        else {
          drupal_set_message(t("Encountered an error when trying to fetch categories from @name. Error @code : @message",
            array('@name' => $server['name'], '@code' => $response->code, '@message' => $response->error)));
          continue;
        }
      }

      if (is_array($categories_raw) AND !empty($categories_raw)) {
        $categories[$url] = $categories_raw;
      }
    }

    // Cache this for 24 hours.
    cache()->set($cid, $categories, REQUEST_TIME + config('project_browser.settings')->get('cache_lifetime'));
  }

  return $categories;
}

/**
 * Gets an array of servers to use for fetching results.
 *
 * @param string $use_server
 *   (optional) The server to use. Defaults to 'all'.
 *
 * @return array
 *   Returns an associative array of servers, populated from the
 *   project_browser_servers variable, in 'url => name' format.
 */
function project_browser_get_servers($use_server = 'all') {
  $servers = config('project_browser.settings')->get('default_server');

  if ($servers_raw = config('project_browser.settings')->get('project_browser_servers')) {
    // Process the variable and add the servers to the list.
    $custom_servers = array();

    $list = explode("\n", $servers_raw);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    foreach ($list as $position => $text) {
      $method = $name = $url = FALSE;

      $matches = array();
      if (preg_match('/(.*)\|(.*)\|(.*)/', $text, $matches)) {
        $url = $matches[1];
        $method = $matches[2];
        $name = $matches[3];
        $custom_servers[$url] = array('name' => $name, 'method' => $method);
      }
    }

    $servers = array_merge($servers, $custom_servers);
  }

  // Filter out servers if necessary.
  if ($use_server !== 'all') {
    $i = 0;
    foreach ($servers as $url => $server) {
      if ($use_server != $i) {
        unset($servers[$url]);
      }
      $i += 1;
    }
  }

  return $servers;
}

/**
 * Uses the project status url to get the available releases for a project.
 *
 * @param array $project
 *   The project to get the releases for.
 *
 * @return array|false
 *   An array of releases for this project, or FALSE if it can't be found.
 */
function project_browser_get_project_release_data(array $project) {
  $releases = array();
  $project['project_type'] = $project['type'];
  $project['includes'] = array();

  // Build the releases cache for this project.
  module_load_include('inc', 'update', 'update.fetch');
  if (_update_process_fetch_task($project)) {
    $data = _update_cache_get('available_releases::' . $project['name']);
    if (isset($data->data) AND isset($data->data['releases']) AND is_array($data->data['releases'])) {
      return $data->data;
    }
  }

  return FALSE;
}

/**
 * Downloads and installs a project using the update module.
 *
 * @todo - Use this new method once the patch in
 *   http://drupal.org/node/1846078 is committed.
 *
 * @param string $url
 *   The url of the release download.
 *
 * @return array
 *   An array indicating whether or not this was successful, and an error
 *   message if applicable.
 */
/**
function project_browser_download_project($url) {
  module_load_include('inc', 'update', 'update.manager');
  module_load_include('inc', 'update', 'update.authorize');
  // Download the file.
  $local_cache = update_manager_file_get($url);
  if (!$local_cache) {
    return array(
      'success' => FALSE,
      'message' => t('Unable to retrieve Drupal project from %url.', array('%url' => $url)),
    );
  }

  try {
    if ($arguments = update_manager_install_project($local_cache)) {
      extract($arguments);
      try {
        $context = array();
        watchdog('debug', '<pre>' . print_r($arguments, TRUE) . '</pre>');
        update_authorize_batch_copy_project($project, $updater_name, $local_url, $filetransfer, $context);
        return array(
          'success' => TRUE,
        );
      }
      catch (UpdaterException $e) {
        return array(
          'success' => FALSE,
          'message' => $e->getMessage(),
        );
      }
    }
    else {
      return array(
        'success' => FALSE,
        'message' => t('The project could not be installed.'),
      );
    }
  }
  catch (Exception $e) {
    return array(
      'success' => FALSE,
      'message' => $e->getMessage(),
    );
  }
}
*/

/**
 * Downloads and installs a project using the update module.
 *
 * @param string $url
 *   The url of the release download.
 *
 * @return array
 *   An array indicating whether or not this was successful, and an error
 *   message if applicable.
 */
function project_browser_download_project($url) {
  module_load_include('inc', 'update', 'update.manager');
  // Download the file.
  $local_cache = update_manager_file_get($url);
  if (!$local_cache) {
    return array(
      'success' => FALSE,
      'message' => t('Unable to retrieve Drupal project from %url.', array('%url' => $url)),
    );
  }

  // Try to extract it.
  $directory = _update_manager_extract_directory();
  try {
    $archive = update_manager_archive_extract($local_cache, $directory);
  }
  catch (Exception $e) {
    return array(
      'success' => FALSE,
      'message' => $e->getMessage(),
    );
  }
  $files = $archive->listContents();
  if (!$files) {
    return array(
      'success' => FALSE,
      'message' => t('Provided archive contains no files.'),
    );
  }

  $project = strtok($files[0], '/\\');

  $archive_errors = update_manager_archive_verify($project, $local_cache, $directory);
  if (!empty($archive_errors)) {
    if (!empty($archive_errors)) {
      foreach ($archive_errors as $error) {
        drupal_set_message(check_plain($error), 'error');
      }
    }
    return array(
      'success' => FALSE,
      'message' => array_shift($archive_errors),
    );
  }

  // Make sure the Updater registry is loaded.
  drupal_get_updaters();

  $project_location = $directory . '/' . $project;
  try {
    $updater = Updater::factory($project_location);
  }
  catch (Exception $e) {
    return array(
      'success' => FALSE,
      'message' => $e->getMessage(),
    );
  }

  try {
    $project_title = Updater::getProjectTitle($project_location);
  }
  catch (Exception $e) {
    return array(
      'success' => FALSE,
      'message' => $e->getMessage(),
    );
  }

  if ($updater->isInstalled()) {
    return array(
      'success' => FALSE,
      'message' => t('%project is already installed.', array('%project' => $project_title)),
    );
  }

  $project_real_location = drupal_realpath($project_location);
  $updater_name = get_class($updater);

  if (fileowner($project_real_location) == fileowner(conf_path())) {
    module_load_include('inc', 'update', 'update.authorize');
    $filetransfer = new Local(DRUPAL_ROOT);

    // Initialize some variables in the Batch API $context array.
    $updater = new $updater_name($project_real_location);

    try {
      if ($updater->isInstalled()) {
        // This is an update.
        $tasks = $updater->update($filetransfer);
      }
      else {
        $tasks = $updater->install($filetransfer);
      }
    }
    catch (UpdaterException $e) {
      return array(
        'success' => FALSE,
        'message' => t('Error installing / updating. Error: @error', array('@error' => $e->getMessage())),
      );
    }
  }
  else {
    return array(
      'success' => FALSE,
      'message' => t('Permissions are not set up properly.'),
    );
  }

  return array(
    'success' => TRUE,
  );
}

/**
 * Installs a single release of a project during batch, for example.
 *
 * @param string $release_name
 *   The name of the release, such as '7.x-1.2'.
 * @param array $project
 *   The project data array.
 * @param array &$context
 *   The context of the batch so that the results can be reported.
 */
function _project_browser_batch_install_release($release_name, array $project, array &$context) {
  module_load_include('inc', 'project_browser', 'project_browser.pages');
  $release = project_browser_get_release($release_name, $project);

  $result = project_browser_download_project($release['download_link']);

  if ($result['success']) {
    $context['results']['successes'][] = t('Successfully installed %project.', array('%project' => $project['title']));
    $context['message'] = t('Installed %project...', array('%project' => $project['title']));

    // Add this to the session variable and remove it from the install_queue
    // variable.
    $_SESSION['project_browser_installed_projects'][$project['name']] = $project;
    unset($_SESSION['project_browser_install_list'][$project['name']]);
  }
  else {
    watchdog('project_browser', 'There was an error while installing %project.
      !message',
      array('%project' => $project['title'], '!message' => $result['message']), WATCHDOG_ERROR);
    $context['results']['failures'][] = t('Error installing %project. Errors have been logged.',
      array('%project' => $project['title']));
    $context['message'] = t('Error installing %project. !message',
      array('%project' => $project['title'], '!message' => $result['message']));
  }
}

/**
 * Shows a message and finishes up the batch.
 *
 * If there were any errors, they are reported here with drupal_set_message().
 * The user is then redirected to the select versions page if there were errors,
 * the install dependencies page if there were any detected missing
 * dependencies, or the enable modules page if there were no errors.
 *
 * @param bool $success
 *   Whether or not the whole operation was successful.
 * @param array $results
 *   An array of messages about any failures.
 * @param array $operations
 *   An array of operations that need to be performed.
 */
function _project_browser_batch_install_releases_finished($success, array $results, array $operations) {
  drupal_get_messages();

  // Restore the maintenance mode to what it was at the start.
  if (isset($_SESSION['maintenance_mode'])) {
    config('system.maintenance')->set('enabled', $_SESSION['maintenance_mode'])->save();
    unset($_SESSION['maintenance_mode']);
  }

  unset($_SESSION['project_browser_install_releases_list']);
  if ($success) {
    if (!empty($results)) {
      if (!empty($results['failures'])) {
        drupal_set_message(format_plural(count($results['failures']), 'Failed to install one project.', 'Failed to install @count projects.'), 'error');
      }
    }
  }
  else {
    drupal_set_message(t('Error installing projects.'), 'error');
    drupal_goto('admin/modules/project-browser/install/select_versions');
  }

  $projects = project_browser_get_installed_projects();
  $missing = project_browser_get_missing_dependencies($projects);
  // If there are missing dependencies, go to install dependencies.
  if (count($missing) > 0) {
    drupal_goto('admin/modules/project-browser/install/install_dependencies');
  }
  else {
    drupal_goto('admin/modules/project-browser/install/enable');
  }
}

/**
 * Gets the dependencies for installed projects.
 *
 * @param array $projects
 *   An array of projects to get the missing dependencies for.
 *
 * @return array
 *   An array of missing dependencies, if any were detected.
 */
function project_browser_get_missing_dependencies(array $projects) {
  $modules = system_rebuild_module_data();

  $missing = array();

  foreach ($projects as $project) {
    if ($project['type'] == 'module') {
      $dependency_check = TRUE;
      $dependencies = array();
      if (isset($modules[$project['name']])) {
        foreach ($modules[$project['name']]->info['dependencies'] as $dependency) {
          if (!isset($modules[$dependency])) {
            $dependencies[] = $dependency;
          }
        }
        if (count($dependencies) > 0) {
          $missing[$project['name']] = $dependencies;
        }
      }
      else {
        drupal_set_message(t('There was an error getting information for @module',
          array('@module' => $project['name'])), 'error');
      }
    }
  }

  return $missing;
}
