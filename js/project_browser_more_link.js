/**
 * @file
 * Makes the 'More' button toggle the display of the full project texts.
 *
 * The project descriptions are by default trimmed to a certain height. When
 * the user clicks the more link, then the full text is shown.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.projectBrowserMoreLink = {
    attach: function (context, settings) {
      // The height of the content block when it's not expanded
      var adjustheight = 80;
      // The "more" link text
      var moreText = Drupal.t('More');
      // The "less" link text
      var lessText = Drupal.t('Less');

      $(".project-information .project-description").each(function(index) {
        if ($(this).height() > adjustheight)
        {
          $(this).css('height', adjustheight).css('overflow', 'hidden');
          $(this).parents(".project-information").append('<a href="#" class="show-more"></a>');
        }
      });

      $("a.show-more").text(moreText);

      $(".show-more").toggle(function() {
          $(this).parents("div:first").find(".project-description").css('height', 'auto').css('overflow', 'visible');
          $(this).text(lessText);
        }, function() {
          $(this).parents("div:first").find(".project-description").css('height', adjustheight).css('overflow', 'hidden');
          $(this).text(moreText);
      });
    }
  };

})(jQuery);
