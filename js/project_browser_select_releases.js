/**
 * @file
 * Shows the default Select Releases page until 'Show All Releases' is clicked.
 */
(function ($) {

  "use strict";

  Drupal.behaviors.projectBrowserSelectReleases = {
    attach: function (context, settings) {
      $('.project-browser-releases-wrapper').hide();
      $('.project-browser-selected-release').show();

      $('.project-browser-show-releases-link').click(function() {
        var target = $(this).attr('rel');
        $('.project-browser-release-' + target).show();
        $('.project-browser-selected-release-' + target).hide();
      })
    }
  };

})(jQuery);
