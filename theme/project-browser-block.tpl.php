<?php
/**
 * @file
 * Default theme implementation to display a block on the projects browser page.
 *
 * Available variables:
 * - $title: The title of the block.
 * - $content: The content of the block.
 *
 * @see project_browser_preprocess_project_browser_block()
 * @ingroup themeable
 */
?>
<div class="project_browser_block">
  <?php print render($title_prefix); ?>
  <h2><?php print $title; ?></h2>
  <?php print render($title_suffix); ?>
  <div class="content">
    <?php print $content; ?>
  </div>
</div>
